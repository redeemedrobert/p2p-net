#include "udpcontroller.h"

// UdpController implements the reliability features of TCP using the UDP protocol. It also implements (in the future) public key cryptography via UDP.

UdpController::UdpController(QObject* parent) : QUdpSocket(parent) {
    // bind QUdpSocket to port 7777. Will remove this in the future
    // so that a random port is given by the network stack, for security
    // and assurance of port availability
    bind(7777, QAbstractSocket::ShareAddress);

    // wire the udpWorker timer
    connect(&udpWorker, &QTimer::timeout, this, &UdpController::workerLoop);

    // read datagarms as soon as they arrive
    connect(this, &QUdpSocket::readyRead, this, &UdpController::readDatagramQueue);

    // start looking for sessions/messages to process
    startWorker();

    // create public and private keys for host
    enc = QRSAEncryption(QRSAEncryption::RSA_2048);
    enc.generatePairKey(publicKey, privateKey);
}

void UdpController::startWorker() {
    // check to see if timer is alread running; start timer if not
    if(!udpWorker.isActive())
        udpWorker.start(250);   // run workerLoop() every 250ms. I don't think there's any reason
                                // it needs to be run any more often than every 1/4 of a second.
}

void UdpController::readDatagramQueue() {
    while(hasPendingDatagrams()) {

        QNetworkDatagram dg = receiveDatagram(-1); // -1 reads the entire datagram, regardless of size
        QJsonDocument doc = QJsonDocument::fromJson(dg.data());

        // qDebug() << "Received datagram, payload: " << dg.data();

        if(doc.isNull()) continue; // we don't want anything that isn't valid JSON.
        if(!doc.isObject()) continue; // it must also be a valid JSON object.

        QJsonObject obj = doc.object();

        if(obj.value("messageType") == QJsonValue::Undefined) continue; // all packets must have a messageType
        qDebug() << "received message of type" << obj.value("messageType").toString();

        if(obj.value("messageType") == "handshake") {
            QString sessionId = obj.value("sessionId").toString();
            if(sessionExists(sessionId) != udpSessions.end()) continue; // session already exists, so handshake was already initiated. This is a duplicate, discard
            createSession(obj.value("sessionId").toString(), QHostAddress(dg.senderAddress().toIPv4Address()).toString(), QString::number(dg.senderPort()),
                          UdpSessionState::SESSION_STATE_HANDSHAKE_RESPOND, QByteArray::fromHex(obj.value("publicKey").toString().toUtf8()));
        }

        if(obj.value("messageType") == "handshake-respond") {
            QString sessionId = obj.value("sessionId").toString();
            vector<UdpSession*>::iterator pos = sessionExists(sessionId);
            if(pos==udpSessions.end()) continue; // handshake repsonse to a non existent session; discard
            (*pos) -> sessionState = UdpSessionState::SESSION_STATE_DUPLEX;
            qDebug() << __FUNCTION__ << "Full duplex!";
            // handshake finished, remove handshake message from session's message queue
            vector<UdpMessage*>::iterator mPos = messageOfTypeExists((*pos), "handshake");
            if(mPos != (*pos) -> messages.end())
                eraseMessage((*pos), (*mPos));
            QJsonObject newObject;
            newObject.insert("messageType", "handshake-confirm");
            newObject.insert("sessionId", (*pos)->uuid);
            createMessage((*pos), QJsonDocument(newObject).toJson(), false);
            emit connectionSucceeded((*pos)->destIp);
        }

        if(obj.value("messageType") == "handshake-confirm") {
            QString sessionId = obj.value("sessionId").toString();
            vector<UdpSession*>::iterator pos = sessionExists(sessionId);
            if(pos==udpSessions.end()) continue; // handshake repsonse to a non existent session; discard
            if((*pos)  -> sessionState == UdpSessionState::SESSION_STATE_DUPLEX) continue; // already in duplex, discard
            (*pos) -> sessionState = UdpSessionState::SESSION_STATE_DUPLEX;
            qDebug() << __FUNCTION__ << "Full duplex-confirmed!";
            // handshake finished, remove handshake-respond message from session's message queue
            vector<UdpMessage*>::iterator mPos = messageOfTypeExists((*pos), "handshake-respond");
            if(mPos != (*pos) -> messages.end())
                eraseMessage((*pos), (*mPos));
            emit connectionSucceeded((*pos)->destIp);
            QJsonObject newObject;
            newObject.insert("messageType", "encrypt");
            newObject.insert("sessionId", (*pos)->uuid);
            newObject.insert("publicKey", QString(publicKey.toHex()));
            createMessage((*pos), QJsonDocument(newObject).toJson(), false);
        }

        if(obj.value("messageType") == "encrypt") {
            QString sessionId = obj.value("sessionId").toString();
            vector<UdpSession*>::iterator pos = sessionExists(sessionId);
            if(pos==udpSessions.end()) continue; // handshake repsonse to a non existent session; discard
            if(obj.value("publicKey") == QJsonValue::Null) continue; // do not accept any handshakes that do not have a publicKey
            (*pos) -> sessionState = UdpSessionState::SESSION_STATE_ENCRYPTED;
            (*pos) -> publicKey = QByteArray::fromHex(obj.value("publicKey").toString().toUtf8());
            qDebug() << __FUNCTION__ << "Encrypted!";
            // handshake finished, remove handshake message from session's message queue
            vector<UdpMessage*>::iterator mPos = messageOfTypeExists((*pos), "handshake-confirm");
            if(mPos != (*pos) -> messages.end())
                eraseMessage((*pos), (*mPos));
            QJsonObject newObject;
            newObject.insert("messageType", "encrypt-confirm");
            newObject.insert("sessionId", (*pos)->uuid);
            newObject.insert("publicKey", QString(publicKey.toHex()));
            QJsonDocument newMessage = QJsonDocument(newObject);
            createMessage((*pos), newMessage.toJson(), false);
            emit connectionEncrypted((*pos)->destIp);
        }

        if(obj.value("messageType") == "encrypt-confirm") {
            QString sessionId = obj.value("sessionId").toString();
            qDebug() << sessionId;
            vector<UdpSession*>::iterator pos = sessionExists(sessionId);
            if(pos==udpSessions.end()) continue; // handshake repsonse to a non existent session; discard
            if((*pos)->sessionState == UdpSessionState::SESSION_STATE_ENCRYPTED) continue; // already encrypted, this is a duplicate. Discard
            if(obj.value("publicKey") == QJsonValue::Null) continue; // do not accept any handshakes that do not have a publicKey
            (*pos) -> sessionState = UdpSessionState::SESSION_STATE_ENCRYPTED;
            (*pos) -> publicKey = QByteArray::fromHex(obj.value("publicKey").toString().toUtf8());
            qDebug() << __FUNCTION__ << "Encrypted!";
            // handshake finished, remove handshake message from session's message queue
            vector<UdpMessage*>::iterator mPos = messageOfTypeExists((*pos), "encrypt");
            if(mPos != (*pos) -> messages.end())
                eraseMessage((*pos), (*mPos));
            // this message will be allowed to send 5 times, since there will be no response and we won't know if it's received
            emit connectionEncrypted((*pos)->destIp);
        }

        if(obj.value("messageType") == "multipart") {
            QString sessionId = obj.value("sessionId").toString();
            vector<UdpSession*>::iterator pos = find_if(
                        udpSessions.begin(),
                        udpSessions.end(),
                        [&sessionId](UdpSession* s) -> bool {
                            return (s->uuid == sessionId);
            });
            if(pos == udpSessions.end()) continue; // session doesn't exist or was already erased, disregard
            if((*pos)->sessionState < UdpSessionState::SESSION_STATE_DUPLEX) continue; // we only want multipart messages once duplex or encrypted
            QString messageId = obj.value("messageId").toString();
            vector<UdpMessage*>::iterator mPos = find_if(
                        (*pos)->messages.begin(),
                        (*pos)->messages.end(),
                        [&messageId](UdpMessage* m) -> bool {
                            return (m->uuid == messageId);
            });
            if(mPos == (*pos)->messages.end()) // message doesn't exist, add it to the session's queue
                createMessage((*pos), dg.data(), true);
            else
                processMultipart((*pos), (*mPos), dg.data()); // message exists, process it to add payload to existing message
        }

        if(obj.value("messageType") == "close") {
            QString sessionId = obj.value("sessionId").toString();
            vector<UdpSession*>::iterator pos = find_if(
                        udpSessions.begin(),
                        udpSessions.end(),
                        [&sessionId](UdpSession* s) -> bool {
                            return (s->uuid == sessionId);
            });
            if(pos == udpSessions.end()) continue; // session doesn't exist or was already erased, disregard
            emit disconnectedFromPeer((*pos)->destIp);
            eraseSession(*pos);
        }

        if(obj.value("messageType") == "encrypted") {
            vector<UdpSession*>::iterator pos = sessionExists(obj.value("sessionId").toString());
            if(pos == udpSessions.end()) continue; // not from a valid session, discard
            emit decryptedMessage(enc.decode(QByteArray::fromHex(obj.value("payload").toString().toUtf8()), privateKey));
        }

    } // while(hasPendingDatagrams())
}

void UdpController::processSessions() {
    for(UdpSession* s : udpSessions) {
        if(s->sessionState == UdpSessionState::SESSION_STATE_HANDSHAKE) {
            if(messageOfTypeExists(s, "handshake") != s->messages.end()) continue;
            QJsonObject obj;
            obj.insert("messageType", "handshake");
            obj.insert("sessionId", s->uuid);
            QJsonDocument payload = QJsonDocument(obj);
            createMessage(s, payload.toJson(), false);
            emit connecting(s->destIp);
            continue;
        }

        if(s->sessionState == UdpSessionState::SESSION_STATE_HANDSHAKE_RESPOND) {
            if(messageOfTypeExists(s, "handshake-respond") != s->messages.end()) continue;
            QJsonObject obj;
            obj.insert("messageType", "handshake-respond");
            obj.insert("sessionId", s->uuid);
            QJsonDocument payload = QJsonDocument(obj);
            createMessage(s, payload.toJson(), false);
            emit connecting(s->destIp);
            continue;
        }
    }
}

void UdpController::processMessages() {
    for(UdpSession* session : udpSessions) {
        if(!session->messages.size()) continue; // don't bother running the loop if there's no messages
        for(UdpMessage* message : session->messages) {
            QJsonDocument mDoc = QJsonDocument::fromJson(message->payload.toUtf8());
            if(mDoc.isNull()) {
                // the JSON was corrupted. Get rid of it
                eraseMessage(session, message);
                continue;
            }
            if((message -> tries) < 6) {
                time_t timeNow = time(nullptr);
                // send the message if it hasn't been sent or if 5 seconds has elapsed since it was last sent
                if(message -> lastSent == 0 || timeNow - (message -> lastSent) >= 5) {
                    // emit connecting(session->destIp);
                    qDebug() << "messageType" << QJsonDocument::fromJson(message->payload.toUtf8()).object().value("messageType");
                    // we don't want the datagram to be sent on the last try, we just want the message to remain in the queue
                    // during this last 5 second period in case the recipient responds during that period
                    if((message->tries) < 5) sendDatagram(session, message);
                    else ++message -> tries; // tries needs to be increased so message is erased without being sent again
                } // if(message -> lastSent == 0 || (message -> lastSent) - timeNow >= 5)
            } else { // if(message -> tries < 6)
                // if still not sent after 5 tries/25 seconds, discard message
                eraseMessage(session, message);
                if(session->sessionState < UdpSessionState::SESSION_STATE_DUPLEX) {
                    // if delivery failure was for session in handshake, discard session
                    emit connectionFailed(session->destIp);
                    eraseSession(session);
                }
            } // if(mObj.value("messageType") == "handshake-respond")*/
        }
    }
}

void UdpController::processMultipart(UdpSession* session, UdpMessage* message, QString payload) {

}

UdpSession* UdpController::createSession(QString sessionId, QString destIp, QString destPort, UdpSessionState state, QByteArray l_publicKey) {
    qDebug() << "Creating session with " << destIp;
    UdpSession* s = new UdpSession();
    time_t timeNow = time(nullptr);
    s->uuid = sessionId.isEmpty() ? QUuid::createUuid().toString() : sessionId;
    s->destIp = destIp;
    s->destPort = destPort;
    s->sessionState = state;
    s->publicKey = l_publicKey;
    s->startTime = timeNow;
    udpSessions.emplace_back(s);
    return s;
}

void UdpController::createMessage(UdpSession* session, QString payload, bool incoming) {
// the old function, for reference:
//    UdpMessage* m = new UdpMessage();
//    m->sessionId = session->uuid;
//    m->payload = payload;
//    m->incoming = incoming;
//    if(incoming == false) {
//        m->uuid = QUuid::createUuid().toString();
//    } else {
//        QJsonDocument mDoc = QJsonDocument::fromJson(payload.toUtf8());
//        if(mDoc.isNull()) {
//            qDebug() << "bad JSON in payload from incoming message";
//            return;
//        }
//        QJsonObject mObj = mDoc.object();
//        m->uuid = mObj.value("messageId").toString();
//    }
//    session->messages.emplace_back(m);
    QJsonDocument doc = QJsonDocument::fromJson(payload.toUtf8());
    if(doc.isNull()) return; // payload should always be valid JSON
    QJsonObject obj = doc.object();
    if(obj.isEmpty()) return; // do not create empty messages
    UdpMessage* m = new UdpMessage();
    m->sessionId = session->uuid;
    m->incoming = incoming;
    if(incoming) { // createMessage is only called on incoming datagrams after duplex is established, when all messages are likely multipart
        m->uuid = obj.value("messageId").toString();
        if(obj.value("messageType").toString() == "multipart") {
            m->totalSequences = obj.value("totalSequences").toInt();
            m->sequenceBuffer.resize(m->totalSequences);
            m->sequenceBuffer.at(obj.value("sequenceId").toInt()) = obj.value("payload").toString();
        }
    } else { // used for handshakes and other stuff in the future
        m->uuid = QUuid::createUuid().toString();
        if(payload.size() > 512) {
            qint16 totalSequences = payload.size() / 512; // 512 is the maximum payload size we want
            if(payload.size() % 512 > 0) ++totalSequences; // totalSequences must be a whole number that captures all sequences
            m->totalSequences = totalSequences;
        }
        m->payload = QJsonDocument(obj).toJson();
    }
    session->messages.emplace_back(m);
}

void UdpController::sendDatagram(UdpSession* session, UdpMessage* message) {
    QNetworkDatagram dg;
    dg.setDestination(QHostAddress(session->destIp), session->destPort.toInt());
    dg.setHopLimit(255);
    dg.setInterfaceIndex(0);
    if(message->totalSequences > 0) { // break the payload into sequences and send all of them
        qint16 i;
        for(i = 0; i < message->totalSequences; ++i) {
            QJsonObject obj;
            obj.insert("messageType", "multipart");
            obj.insert("sequenceId", i);
            obj.insert("totalSequences", message->totalSequences);
            obj.insert("payload", message->payload.mid(i*512, i*512+512)); // need to get rid of magic number
            dg.setData(QJsonDocument(obj).toJson());
            qDebug () << "sendDg" << writeDatagram(dg); // send the datagram and log its size
        }
    } else {
        dg.setData(message->payload.toUtf8());
        // better to let this be set automatically by the stack
        // dg.setSender(localAddress(), localPort());
        qint64 sendDg = writeDatagram(dg);
        qDebug() << "sendDg:" << sendDg;
    }
    ++message -> tries;
    message -> lastSent = time(nullptr);
}

void UdpController::sendEncryptedMessage(QString ip, QString message) {
    for(UdpSession* s : udpSessions) {
        if(s->destIp == ip) {
            QJsonObject obj;
            obj.insert("messageType", "encrypted");
            obj.insert("sessionId", s->uuid);
            obj.insert("payload", QString(enc.encode(message.toUtf8(), s->publicKey).toHex()));
            QNetworkDatagram dg;
            dg.setDestination(QHostAddress(s->destIp), s->destPort.toInt());
            dg.setData(QJsonDocument(obj).toJson());
            dg.setHopLimit(255);
            dg.setInterfaceIndex(0);
            // dg.setSender(localAddress(), localPort());
            qint64 sendDg = writeDatagram(dg);
            qDebug() << "sendDg(encrypted): " << sendDg;
        }
    }
}

vector<UdpMessage*>::iterator UdpController::messageOfTypeExists(UdpSession* session, const QString& messageType) {
    return find_if(
            session -> messages.begin(),
            session -> messages.end(),
            [&messageType](UdpMessage* m) -> bool {
                QJsonDocument mDoc =  QJsonDocument::fromJson(m->payload.toUtf8());
                if(mDoc.isNull()) return false;
                QJsonObject mObj = mDoc.object();
                return (mObj.value("messageType")!=QJsonValue::Undefined &&
                    mObj.value("messageType") == messageType);
            });
}

vector <UdpSession*>::iterator UdpController::sessionExists(const QString& uuid) {
    return find_if(
                udpSessions.begin(),
                udpSessions.end(),
                [&uuid] (UdpSession* s) -> bool {
                    return s->uuid == uuid;
                });
}

void UdpController::eraseSession(UdpSession* session) {
    vector<UdpSession*>::iterator pos = std::find(
                udpSessions.begin(),
                udpSessions.end(),
                session
                );
    if(pos != udpSessions.end()) {
        // erase all messages in session to avoid memory leak
        for(UdpMessage* m : session->messages) {
            eraseMessage(session, m);
        }
        udpSessions.erase(pos);
        qDebug() << "erasing session" << session->uuid;
        delete session;
        session = nullptr;
    }
}

void UdpController::eraseMessage(UdpSession* session, UdpMessage* message) {
    vector<UdpMessage*>::iterator pos = std::find(
                session->messages.begin(),
                session->messages.end(),
                message
                );
    if(pos != session->messages.end())
        session->messages.erase(pos);
    qDebug() << "Erasing message" << message->uuid << "of type" << QJsonDocument::fromJson(message->payload.toUtf8()).object().value("messageType").toString();
    delete message;
    message = nullptr;
}

void UdpController::workerLoop() {
    if(udpSessions.size() > 0) { // skip running the functions if there's no UDP sessions
        processSessions();
        processMessages();
    }
}

void UdpController::connectToPeer(QString ip, QString port) {
    createSession("", ip, port, UdpSessionState::SESSION_STATE_HANDSHAKE, publicKey);
    startWorker();
}

void UdpController::closePeerConnection(QString ip) {
    for(UdpSession* s : udpSessions) {
        if(s->destIp == ip) {
            QJsonObject obj;
            QNetworkDatagram dg;
            obj.insert("messageType", "close");
            obj.insert("sessionId", s->uuid);
            dg.setDestination(QHostAddress(s->destIp), s->destPort.toInt());
            dg.setData(QJsonDocument(obj).toJson());
            dg.setHopLimit(255);
            dg.setInterfaceIndex(0);
            writeDatagram(dg); writeDatagram(dg); writeDatagram(dg); writeDatagram(dg); writeDatagram(dg);
            // send five times for kind-of-reliability. Since the session is about to be erased, we can't keep it in
            // the session's message queue
            eraseSession(s);
            emit disconnectedFromPeer(ip);
        }
    }
}
