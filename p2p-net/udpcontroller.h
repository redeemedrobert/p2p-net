#ifndef UDPCONTROLLER_H
#define UDPCONTROLLER_H

#include <QTimer> // for workerLoop
#include <QUdpSocket> // underlying network interface of UdpController
#include <QUuid> // generates session IDs and message IDs
#include <vector> // for storage of session and message pointers
#include <ctime> // for timestamping messages and sessions
#include <QNetworkDatagram> // for datagram construction
#include <QJsonDocument> // for structuring datagram payloads
#include <QJsonObject> // ''
#include <algorithm> // for vector convenience functions
#include <QTextCodec>
#include <qrsaencryption.h>

using std::vector;
using std::find_if;

struct UdpMessage {
    QString uuid; // uuid of the message, set by the sender of the message
    QString sessionId; // which session the message belongs to
    bool incoming; // true if incoming, false if outgoing
    QString payload; // payload of the message
    qint16 totalSequences { 0 }; // total amount of sequences the payload is composed of. Set by the sender
    vector <QString> sequenceBuffer; // buffer for incoming/outgoing multi-part payloads
    vector <qint16> sequenceIds; // Ids that match array position with the sequence's position in sequenceBuffer
    time_t lastSent { 0 }; // for outgoing messages, the time at which the last attempt to send was made
    qint16 tries { 0 };   // for outgoing messages, the number of attempts that have been made to send the message
                    // for incoming messages, the number of attempts that have been made to have missing
                    // sequences resent
};

enum UdpSessionState {
    SESSION_STATE_HANDSHAKE,        // sending a handshake
    SESSION_STATE_HANDSHAKE_RESPOND,// responding to a handshake
    SESSION_STATE_HANDHSAKE_FAILED, // handshake failed, retry
    SESSION_STATE_DUPLEX,           // handshake completed, session is sending/receiving
    SESSION_STATE_ENCRYPTED         // public keys have been exchanged, all communications now encrypted
};

struct UdpSession { // a session is generated for every connetion between two peers or with the STUN server
    QString uuid; // unique identifier for the connection/transaction being facilitated by this job
    QString destIp; // destination address if outgoing, source address if incoming
    QString destPort; // ''
    UdpSessionState sessionState; // from UdpJobType enumerator
    bool persistent; // true if persistent connection, false if single transaction
    bool handshakeComplete; // true if handshake has been completed
    time_t startTime; // time_t at which the session started/attempted to start
    QByteArray publicKey; // destination public key if outgoing, host public key if incoming
    vector <UdpMessage*> messages; // array of all active messages being exchanged during session
};

class UdpController : public QUdpSocket
{
    Q_OBJECT

public:
    UdpController(QObject* parent);

signals:
    void connectionFailed(QString ip); // when an outgoing handshake fails
    void connecting(QString ip); // when a handshake datagram is about to be sent
    void connectionSucceeded(QString ip); // when handshake-respond or handshake-confirm are received
    void connectionEncrypted(QString ip); // once the public key exchange is completed
    void disconnectedFromPeer(QString ip); // after intentional disconnection
    void decryptedMessage(QString message); // once an encrypted message is decoded, the decoded message is "QString message"

public slots:
    void startWorker(); // start workerLoop
    void workerLoop(); // reads datagram queue, processes sessions, and then processes messages
    void connectToPeer(QString ip, QString port); // slot for use from QML; starts handshake with peer
    void closePeerConnection(QString ip); // slot for use from QML, ends all sessions to peer
    void sendEncryptedMessage(QString ip, QString message);

private:
    QTimer udpWorker; // timer to run workerLoop ever 250ms
    vector <UdpSession*> udpSessions; // pointers to all active sessions
    void readDatagramQueue(); // read the UdpSocket's datagram queue and act on messages if needed, or discard garbage
    void processSessions(); // check status of all sessions and act on them if needed
    void processMessages(); // process all messages in all sessions' queues
    UdpSession* createSession(QString sessionId, QString destIp, QString destPort, UdpSessionState state, QByteArray publicKey); // create a session!
    void createMessage(UdpSession* session, QString payload, bool incoming); // add message to a sessions message queue
    void eraseMessage(UdpSession* session, UdpMessage* message); // delete message from memory and remove its pointer from queue
    void eraseSession(UdpSession* session); // delete session from memory and remove its pointer in the sessions vector
    void sendDatagram(UdpSession* session, UdpMessage* message); // actually send a datagram from a message queue
    void processMultipart(UdpSession* session, UdpMessage* message, QString payload);
    vector<UdpMessage*>::iterator messageOfTypeExists(UdpSession* session, const QString& messageType); // check if messageType exists in queue
    vector <UdpSession*>::iterator sessionExists(const QString& uuid);
    QRSAEncryption enc;
    QByteArray publicKey, privateKey; // Host's public and private key

};


#endif // UDPCONTROLLER_H
