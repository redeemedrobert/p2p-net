import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15

Page {
    id: page
    // width: 600
    // height: 400

    title: qsTr("SSL Test")

    Column {
        id: column
        anchors.centerIn: parent
        topPadding: 5
        spacing: 5

        Label {
            id: lab
            text: qsTr("Enter an IP address and port to connect to")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextField {
            id: textField
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("0.0.0.0")
        }

        TextField {
            id: port
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("65432")
            text: qsTr("65432")
        }

        RowLayout {
            id: row
            width: parent.width
            visible: true
            spacing: 3
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: button
                text: qsTr("Connect")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: {
                    console.log("Starting SSL Connection");
                    SslClient.sbSslConnect(textField.text, port.text)
                }
                Connections {
                    target: SslClient
                    function onStateChanged(socketState) {
                        switch(socketState) {
                        case 0:
                            button.visible = true
                            break;
                        default:
                            button.visible = false
                        }
                    }
                }
            }
            Button {
                id: cancelButton
                text: qsTr("Abort")
                visible: false;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: SslClient.abort()
                Connections {
                    target: SslClient
                    function onStateChanged(socketState) {
                        switch (socketState) {
                        case 2:
                            cancelButton.visible = true;
                            break;
                        case 3:
                            cancelButton.visible = true;
                            break;
                        default:
                            cancelButton.visible = false;
                        }
                    }
                }
            }
        }

        Label {
            id: label
            text: qsTr("Client status: Not connected")
            anchors.horizontalCenter: parent.horizontalCenter
            Connections {
                target: SslClient
                function onStateChanged(socketState) {
                    switch(socketState) {
                    case 0:
                        label.text = qsTr("Client status: Unconnected");
                        break;
                    case 1:
                        label.text = qsTr("Client status: Lookup");
                        break;
                    case 2:
                        label.text = qsTr("Client status: Connecting");
                        break;
                    case 3:
                        label.text = qsTr("Client status: Connected");
                        break;
                    case 4:
                        label.text = qsTr("Client status: Bound");
                        break;
                    case 6:
                        label.text = qsTr("Client status: Closing");
                        break;
                    default:
                        label.text = qsTr("Client status: Not connected");
                    }
                }
            }
        }

        RowLayout {
            id: messageRow
            visible: false
            width: parent.width
            Connections {
                target: SslClient
                function onStateChanged(socketState) {
                    switch(socketState) {
                    case 3:
                        messageRow.visible = true
                        break;
                    default:
                        messageRow.visible = false
                    }
                }
            }

            TextField {
                id: message
                Layout.fillWidth: true
                placeholderText: "Message..."
            }

            Button {
                text: qsTr("Send")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                onClicked: SslClient.sbSslSend(message.text)
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
