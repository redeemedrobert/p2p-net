import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15

Page {
    id: page
    // width: 600
    // height: 400

    title: qsTr("Home")

    Column {
        id: column
        anchors.centerIn: parent
        topPadding: 5
        spacing: 5

        Label {
            id: lab
            text: qsTr("Enter an IP address and port to connect to")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextField {
            id: textField
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("0.0.0.0")
            Connections {
                target: UdpController
                function onConnectionSucceeded(ip) {
                    textField.text = ip;
                }
            }
        }

        TextField {
            id: port
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("7777")
            text: qsTr("7777")
        }

        RowLayout {
            id: row
            width: parent.width
            visible: true
            spacing: 3
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: button
                text: qsTr("Connect")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: UdpController.connectToPeer(textField.text, port.text);
                Connections {
                    target: UdpController
                    function onConnecting(ip) {
                        button.visible = false;
                    }
                    function onConnectionSucceeded(ip) {
                        button.visible = false;
                    }
                    function onDisconnectedFromPeer(ip) {
                        button.visible = true;
                    }
                    function onConnectionFailed(ip) {
                        button.visible = true;
                    }
                }
            }
            Button {
                id: cancelButton
                text: qsTr("Abort")
                visible: false;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: UdpController.closePeerConnection(textField.text);
                Connections {
                    target: UdpController
                    function onConnecting(ip) {
                        cancelButton.visible = true;
                    }
                    function onConnectionSucceeded(ip) {
                        cancelButton.visible = true;
                    }
                    function onDisconnectedFromPeer(ip) {
                        cancelButton.visible = false;
                    }
                    function onConnectionFailed(ip) {
                        cancelButton.visible = false;
                    }
                }
            }
        }

        Label {
            id: label
            text: qsTr("Status: Not connected")
            anchors.horizontalCenter: parent.horizontalCenter
            Connections {
                target: UdpController
                function onConnecting(ip) {
                    label.text = "Status: Connecting to " + ip;
                }
                function onConnectionFailed(ip) {
                    label.text = "Status: Failed to connect to " + ip;
                }
                function onConnectionSucceeded(ip) {
                    label.text = "Status: Connected to " + ip;
                }
                function onConnectionEncrypted(ip) {
                    label.text = "Status: connected to " + ip + " and encrypted";
                }
                function onDisconnectedFromPeer(ip) {
                    label.text = "Status: Not connected";
                }
            }
        }

        RowLayout {
            id: encLayout
            width: parent.width
            visible: false

            Connections {
                target: UdpController
                function onConnectionEncrypted(ip) {
                    encLayout.visible = true;
                }
                function onConnectionFailed(ip) {
                    encLayout.visible = false;
                }
                function onDisconnectedFromPeer(ip) {
                    encLayout.visible = false;
                }
            }

            TextField {
                id: encField
                Layout.fillWidth: true
                placeholderText: qsTr("Encrypted message")
            }

            Button {
                id: button1
                text: qsTr("Send")
                Layout.fillWidth: false
                onClicked: UdpController.sendEncryptedMessage(textField.text, encField.text)
            }
        }

        Label {
            id: encText
            text: qsTr("Encrypted messages will appear here when received")
            visible: false;
            Connections {
                target: UdpController
                function onConnectionEncrypted(ip) {
                    encText.visible = true;
                }
                function onConnectionFailed(ip) {
                    encLayout.visible = false;
                    encText.text = "Encrypted messages will appear here when received";
                }
                function onDisconnectedFromPeer(ip) {
                    encLayout.visible = false;
                    encText.text = "Encrypted messages will appear here when received";
                }
                function onDecryptedMessage(message) {
                    if(encText.text == "Encrypted messages will appear here when received")
                        encText.text = "";
                    encText.text += message + "\n";
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
