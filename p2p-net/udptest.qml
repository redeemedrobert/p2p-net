import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12
Page {
    id: udppage
    // width: 600
    // height: 400

    title: qsTr("UDP Test")

    Column {
        id: column
        anchors.centerIn: parent
        topPadding: 5
        spacing: 5

        Label {
            id: lab
            text: qsTr("Enter an IP address and port to connect to")
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextField {
            id: textField
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("0.0.0.0")
        }

        TextField {
            id: port
            width: lab.width
            font.styleName: "Bold"
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("7777")
            text: qsTr("7777")
        }

        RowLayout {
            id: row
            width: parent.width
            visible: true
            spacing: 3
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: button
                text: qsTr("Send")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: UdpController.sbSendPackets(textField.text, port.text);
            }
            Button {
                id: cancelButton
                text: qsTr("Abort")
                visible: false;
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: UdpController.sbStopSending();
                Connections {
                    target: UdpController
                    function onSbSending() {
                        button.visible = false;
                        cancelButton.visible = true;
                    }
                    function onSbStopped() {
                        cancelButton.visible = false;
                        button.visible = true;
                    }
                }
            }
        }

        Label {
            id: label
            property int sentPackets: 0;
            text: qsTr("Socket outgoing status: Idle")
            anchors.horizontalCenter: parent.horizontalCenter
            Connections {
                target: UdpController
                function onSbSending() {
                    label.text = qsTr("Socket outgoing status: Sending")
                }
                function onSbSent() {
                    ++label.sentPackets;
                    label.text = qsTr("Socket outgoing status: Sent " + label.sentPackets);
                }

                function onSbStopped() {
                    label.text = qsTr("Socket outgoing status: Idle")
                    label.sentPackets = 0;
                }
            }
        }

        Label {
            id: label2
            text:  qsTr("Socket incoming status: Waiting")
            anchors.horizontalCenter: parent.horizontalCenter
            Connections {
                target: UdpController
                function onReadyRead() {
                    label2.text = qsTr("Socket incoming status: Datagram ready")
                }
                function onSbStopped() {
                    label2.text = qsTr("Socket incoming status: Waiting")
                }
            }
        }
    }
}
